﻿using System.ComponentModel.DataAnnotations;

namespace Ejemplo1ASP.ViewModels
{
    public class OlvidoPasswordViewModel
    {
        [Required(ErrorMessage = "Email Obligatorio")]
        [EmailAddress(ErrorMessage = "El formato del Email no es correcto")]
        public string Email { get; set; }
    }
}
