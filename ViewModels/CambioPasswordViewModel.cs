﻿using System.ComponentModel.DataAnnotations;

namespace Ejemplo1ASP.ViewModels
{
    public class CambioPasswordViewModel
    {
        [Required(ErrorMessage = "Obligatorio")]
        [DataType(DataType.Password)]
        [Display(Name = "Password Actual")]
        public string PasswordActual { get; set; }

        [Required(ErrorMessage = "Obligatorio")]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva Password")]
        public string NuevaPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirma nueva password")]
        [Compare("NuevaPassword", ErrorMessage = "La password nueva y la confirmación no coinciden")]
        public string ConfirmarPassword { get; set; }
    }
}
