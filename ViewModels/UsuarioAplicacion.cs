﻿using Microsoft.AspNetCore.Identity;

namespace Ejemplo1ASP.ViewModels
{
    public class UsuarioAplicacion:IdentityUser
    {
        public string ayudaPass { get; set; }
    }
}
