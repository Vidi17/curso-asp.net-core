﻿using System;
using System.Collections.Generic;

namespace Ejemplo1ASP.ViewModels
{
    public class UsuarioClaimsViewModel
    {
        public UsuarioClaimsViewModel()
        {
            Claims = new List<UsuarioClaim>();
        }

        public string idUsuario { get; set; }
        public List<UsuarioClaim> Claims { get; set; }
    }
}
