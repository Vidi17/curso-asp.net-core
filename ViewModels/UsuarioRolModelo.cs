﻿namespace Ejemplo1ASP.ViewModels
{
    public class UsuarioRolModelo
    {
        public string UsuarioId { get; set; }
        public string UsuarioNombre { get; set; }
        public bool EstaSeleccionado { get; set; }
    }
}
