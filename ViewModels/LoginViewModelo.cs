﻿using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ejemplo1ASP.ViewModels
{
    public class LoginViewModelo
    {
        [Required (ErrorMessage = "Email obligatorio")]
        [EmailAddress]
        public string Email { get; set; }

        [Required (ErrorMessage = "Password obligatoria"), MinLength(4, ErrorMessage = "4 caracteres como mínimo")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Recuerdame")]
        public bool Recuerdame { get; set; }

        public string UrlRetorno { get; set; }

        public IList<AuthenticationScheme> LoginExternos { get; set; }
    }
}
