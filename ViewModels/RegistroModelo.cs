﻿using Ejemplo1ASP.Utilidades;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Ejemplo1ASP.ViewModels
{
    public class RegistroModelo
    {
        [Required(ErrorMessage = "Email obligatorio")]
        [Display(Name = "Email")]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", ErrorMessage = "Formato incorrecto")]
        [EmailAddress]
        [Remote(action: "comprobarEmail", controller: "Cuentas")]
        [ValidarNombreUsuario(usuario: "joder", ErrorMessage = "Palabra no permitida")]
        public string Email { get; set; }

        [Required (ErrorMessage = "Password obligatoria")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Repetir Password")]
        [Compare("Password", ErrorMessage = "La Password y la Password de confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Ayuda Password")]
        public string AyudaPass { get; set; }
    }
}
