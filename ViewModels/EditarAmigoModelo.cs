﻿using Ejemplo1ASP.Models;

namespace Ejemplo1ASP.ViewModels
{
    public class EditarAmigoModelo:CrearAmigoModelo
    {
        public int Id { get; set; }
        public string rutaFotoExistente { get; set; }
    }
}
