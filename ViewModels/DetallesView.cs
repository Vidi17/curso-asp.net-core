﻿using Ejemplo1ASP.Models;

namespace Ejemplo1ASP.ViewModels
{
    public class DetallesView
    {
        public string Titulo { get; set; }
        public string SubTitulo { get; set; }
        public Amigo amigo { get; set; }
    }
}
