﻿namespace Ejemplo1ASP.ViewModels
{
    public class RolUsuarioModelo
    {
        public string RolId { get; set; }
        public string RolNombre { get; set; }
        public bool EstaSeleccionado { get; set; }
    }
}
