﻿using System.ComponentModel.DataAnnotations;

namespace Ejemplo1ASP.ViewModels
{
    public class CrearRolViewModel
    {
        [Required(ErrorMessage = "Este campo es obligatorio")]
        [Display(Name = "Rol")]
        public string NombreRol { get; set; }
    }
}
