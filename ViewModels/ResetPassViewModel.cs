﻿using System.ComponentModel.DataAnnotations;

namespace Ejemplo1ASP.ViewModels
{
    public class ResetPassViewModel
    {
        [Required (ErrorMessage = "Email obligatorio")]
        [EmailAddress]
        public string Email { get; set; }

        [Required (ErrorMessage = "Obligatorio")]
        [DataType (DataType.Password)]
        public string Password { get; set; }

        [DataType (DataType.Password)]
        [Display (Name = "Confirmar Password")]
        [Compare ("Password", ErrorMessage = "Password y su campo de confirmación deben coincidir")]
        public string ConfirmPassword { get; set; }

        public string Token { get; set; }
    }
}
