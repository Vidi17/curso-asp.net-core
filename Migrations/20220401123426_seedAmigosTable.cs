﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ejemplo1ASP.Migrations
{
    public partial class seedAmigosTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Amigos",
                columns: new[] { "Id", "Ciudad", "Email", "Nombre" },
                values: new object[] { 5, 4, "Juande@mail.com", "Juande" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Amigos",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
