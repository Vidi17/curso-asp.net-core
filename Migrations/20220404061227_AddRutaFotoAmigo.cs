﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ejemplo1ASP.Migrations
{
    public partial class AddRutaFotoAmigo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "rutaFoto",
                table: "Amigos",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "rutaFoto",
                table: "Amigos");
        }
    }
}
