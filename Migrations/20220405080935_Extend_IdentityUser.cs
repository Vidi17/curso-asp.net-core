﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ejemplo1ASP.Migrations
{
    public partial class Extend_IdentityUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ayudaPass",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ayudaPass",
                table: "AspNetUsers");
        }
    }
}
