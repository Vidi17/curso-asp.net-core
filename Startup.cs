using Ejemplo1ASP.Models;
using Ejemplo1ASP.Seguridad;
using Ejemplo1ASP.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Ejemplo1ASP
{
    public class Startup
    {
        private IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<AppDbContext>(options => options.UseSqlServer(_configuration.GetConnectionString("ConexionSQL")));
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;

                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            }).AddXmlSerializerFormatters();

            services.AddScoped<IAmigoAlmacen, SQLAmigoRepositorio>();
            services.AddIdentity<UsuarioAplicacion, IdentityRole>(
                options =>
                {
                    options.SignIn.RequireConfirmedEmail = true;
                }).AddErrorDescriber<ErroresCastellano>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Cuentas/Login";
                options.AccessDeniedPath = "/Cuentas/AccesoDenegado";
            });

            services.Configure<IdentityOptions>(opciones =>
            {
                opciones.Password.RequiredLength = 8;
                opciones.Password.RequiredUniqueChars = 3;
                opciones.Password.RequireNonAlphanumeric = false;
            });

            services.AddAuthentication().AddGoogle(opciones =>
            {
                opciones.ClientId = "900330195027-61jc9cjb9iim4aq0m3d2co888j8uacp4.apps.googleusercontent.com";
                opciones.ClientSecret = "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";
            }).AddFacebook(opciones =>
            {
                opciones.AppId = "2269864306501013";
                opciones.AppSecret = "722ca1c66d4f678420c5649cffa5892f";
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("BorrarRolPolicy", policy => policy.RequireClaim("Borrar Rol"));
                /*options.AddPolicy("EditarRolPolicy", policy => policy.RequireAssertion(context =>
                    context.User.IsInRole("Administrador") &&
                    context.User.HasClaim(claim => claim.Type == "Editar Rol" && claim.Value == "true") ||
                    context.User.IsInRole("Dios")
                ));*/

                options.AddPolicy("EditarRolPolicy", policy => policy.AddRequirements(new GestionarAdminRolesyClaims()));
                options.AddPolicy("CrearRolPolicy", policy => policy.RequireClaim("Crear Rol"));

                services.Configure<DataProtectionTokenProviderOptions>(o => o.TokenLifespan = System.TimeSpan.FromHours(1));
            });

            services.AddSingleton<IAuthorizationHandler, PoderEditarSoloOtrosClaimsRoles>();

            services.AddSingleton<ProteccionStrings>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                DeveloperExceptionPageOptions d = new DeveloperExceptionPageOptions
                {
                    SourceCodeLineCount = 2
                };
                app.UseDeveloperExceptionPage(d);
            }
            else if(env.IsProduction() || env.IsStaging())    
            {
                app.UseExceptionHandler("/Error");
                app.UseStatusCodePagesWithRedirects("/Error/{0}");
            }

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=home}/{action=Index}/{id?}");
            });
        }
    }
}
