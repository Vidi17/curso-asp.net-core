﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Ejemplo1ASP.Models
{
    public class AlmacenClaims
    {
        public static List<Claim> todosLosClaims = new List<Claim>()
        {
            new Claim("Crear Rol", "Crear Rol"),
            new Claim("Editar Rol", "Editar Rol"),
            new Claim("Borrar Rol", "Borrar Rol")
        };
    }
}
