﻿using System.Collections.Generic;

namespace Ejemplo1ASP.Models
{
    public interface IAmigoAlmacen
    {
        Amigo DameDatosAmigo(int d);
        List<Amigo> DameTodosLosAmigos();
        Amigo nuevo(Amigo amigo);
        Amigo modificar(Amigo modificarAmigo);
        Amigo borrar(int id);

    }
}
