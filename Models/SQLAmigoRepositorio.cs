﻿using System.Collections.Generic;
using System.Linq;

namespace Ejemplo1ASP.Models
{
	public class SQLAmigoRepositorio:IAmigoAlmacen
	{
		private readonly AppDbContext contexto;
		private List<Amigo> amigosLista;

		public SQLAmigoRepositorio(AppDbContext contexto)
		{
			this.contexto = contexto;
		}

		public Amigo nuevo(Amigo amigo)
		{
			contexto.Amigos.Add(amigo);	
			contexto.SaveChanges();
			return amigo;
		}

		public Amigo modificar(Amigo amigo)
		{
			var employee = contexto.Amigos.Attach(amigo);
			employee.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
			contexto.SaveChanges();
			return amigo;
		}

		public Amigo borrar(int Id)
		{
			Amigo amigo = contexto.Amigos.Find(Id);
			if (amigo != null)
			{
				contexto.Amigos.Remove(amigo);
				contexto.SaveChanges();
			}
			return amigo;
		}

		public List<Amigo> DameTodosLosAmigos()
		{
			amigosLista = contexto.Amigos.ToList<Amigo>();

			return amigosLista;
		}

		public Amigo DameDatosAmigo(int Id)
		{
			return contexto.Amigos.Find(Id);
		}
	}
}
