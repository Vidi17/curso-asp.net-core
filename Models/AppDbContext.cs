﻿using Ejemplo1ASP.ViewModels;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Ejemplo1ASP.Models
{
    public class AppDbContext:IdentityDbContext<UsuarioAplicacion>
	{
		public AppDbContext(DbContextOptions<AppDbContext> options): base(options)
		{
		}

		public DbSet<Amigo> Amigos { get; set;}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
			{
				foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
			}
		}
	}
}
