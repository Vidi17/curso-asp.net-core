﻿namespace Ejemplo1ASP.Models
{
	public enum Provincia
	{
		Ninguna, Coruña, Álava, Albacete, Alicante, Almería, Asturias, Ávila, Badajoz
		, Baleares, Barcelona, Burgos, Cáceres, Cádiz, Cantabria, Castellón, CiudadReal
		, Córdoba, Cuenca, Girona, Granada, Guadalajara, Gipuzkoa, Huelva, Huesca, Jaén
		, LaRioja, LasPalmas, León, Lérida, Lugo, Madrid, Málaga, Murcia, Navarra, Ourense
		, Palencia, Pontevedra, Salamanca, Segovia, Sevilla, Soria, Tarragona, Tenerife
		, Teruel, Toledo, Valencia, Valladolid, Vizcaya, Zamora, Zaragoza, Ceuta, Melilla
	}
}
