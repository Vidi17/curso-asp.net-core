﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Ejemplo1ASP.Seguridad
{
    public class PoderEditarSoloOtrosClaimsRoles : AuthorizationHandler<GestionarAdminRolesyClaims>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            GestionarAdminRolesyClaims peticion)
        {
            var authFilterContext = context.Resource as AuthorizationFilterContext;
            if (authFilterContext == null)
            {
                return Task.CompletedTask;
            }

            string IdUsuarioLoggeado = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

            string adminIdEdicion = authFilterContext.HttpContext.Request.Query["IdUsuario"];

            if (context.User.IsInRole("Administrador") &&
                context.User.HasClaim(claim => claim.Type == "Editar Rol" && claim.Value == "true") && 
                adminIdEdicion.ToLower() != IdUsuarioLoggeado.ToLower())
            {
                context.Succeed(peticion);
            }

            return Task.CompletedTask;
        }
    }
}
