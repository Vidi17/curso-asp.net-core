﻿using Ejemplo1ASP.Models;
using Ejemplo1ASP.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Ejemplo1ASP.Controllers
{
	[Authorize(Roles = "Administrador, Dios")]
    public class AdministracionController:Controller
    {
        private readonly RoleManager<IdentityRole> gestionRoles;
        private readonly UserManager<UsuarioAplicacion> gestionUsuarios;
        private readonly ILogger<AdministracionController> log;

        public AdministracionController(RoleManager<IdentityRole> gestionRoles,
                                        UserManager<UsuarioAplicacion> gestionUsuarios,
                                        ILogger<AdministracionController> log)
        {
            this.gestionRoles = gestionRoles;
            this.gestionUsuarios = gestionUsuarios;
            this.log = log;

        }

        [HttpGet]
        [Route("Administracion/CrearRol")]
        public IActionResult CrearRol()
        {
            return View();
        }

        [HttpPost]
        [Route("Administracion/CrearRol")]
        public async Task<IActionResult> CrearRol(CrearRolViewModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityRole identityRole = new IdentityRole
                {
                    Name = model.NombreRol
                };

                IdentityResult result = await gestionRoles.CreateAsync(identityRole);

                if (result.Succeeded)
                {
                    return RedirectToAction("Roles", "Administracion");
                }

                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(model);
        }

        [HttpGet]
        [Route("Administracion/Roles")]
        public IActionResult ListaRoles()
        {
            var roles = gestionRoles.Roles;
            return View(roles);
        }

        [HttpGet]
        //[Route("Administracion/EditarRol")]
        //[Authorize(Policy = "EditarRolPolicy")]
        public async Task<IActionResult> EditarRol(string id)
        {
            var rol = await gestionRoles.FindByIdAsync(id);

            if (rol == null)
            {
                ViewBag.ErrorMessage = $"El Rol con el Id = {id} no fue encontrado";
                return View("Error");
            }

            var model = new EditarRolViewModel
            {
                Id = rol.Id,
                RolNombre = rol.Name
            };

            foreach (var usuario in gestionUsuarios.Users)
            {
                if (await gestionUsuarios.IsInRoleAsync(usuario, rol.Name))
                {
                    model.Usuarios.Add(usuario.UserName);
                }
            }
            return View(model);
        }

        [HttpPost]
        [Route("Administracion/EditarRol")]
        //[Authorize(Policy = "EditarRolPolicy")]
        public async Task<IActionResult> EditarRol(EditarRolViewModel model)
        {
            var rol = await gestionRoles.FindByIdAsync(model.Id);

            if (rol == null)
            {
                ViewBag.ErrorMessage = $"El Rol con el Id = {model.Id} no fue encontrado";
                return View("Error");
            }
            else
            {
                rol.Name = model.RolNombre;

                var resultado = await gestionRoles.UpdateAsync(rol);

                if (resultado.Succeeded)
                {
                    return RedirectToAction("ListaRoles");
                }

                foreach (var error in resultado.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                return View(model);
            }
        }

        [HttpGet]
        [Route("Administracion/EditarUsuarioRol")]
        public async Task<IActionResult> EditarUsuarioRol(string rolId)
        {
            ViewBag.roleId = rolId;

            var role = await gestionRoles.FindByIdAsync(rolId);

            if (role == null)
            {
                ViewBag.ErrorMessage = $"El rol con el Id = {rolId} no fue encontrado";
                return View("Error");
            }

            var model = new List<UsuarioRolModelo>();

            foreach (var user in gestionUsuarios.Users)
            {
                var usuarioRolModelo = new UsuarioRolModelo
                {
                    UsuarioId = user.Id,
                    UsuarioNombre = user.UserName
                };

                if (await gestionUsuarios.IsInRoleAsync(user, role.Name))
                {
                    usuarioRolModelo.EstaSeleccionado = true;
                }
                else
                {
                    usuarioRolModelo.EstaSeleccionado = false;
                }

                model.Add(usuarioRolModelo);
            }

            return View(model);
        }

        [HttpPost]
        [Route("Administracion/EditarUsuarioRol")]
        public async Task<IActionResult> EditarUsuarioRol(List<UsuarioRolModelo> model, string rolId)
        {
            var rol = await gestionRoles.FindByIdAsync(rolId);

            if (rol == null)
            {
                ViewBag.ErrorMessage = $"El rol con el Id = {rolId} no fue encontrado";
                return View("Error");
            }

            for (int i = 0; i < model.Count; i++)
            {
                var user = await gestionUsuarios.FindByIdAsync(model[i].UsuarioId);

                IdentityResult result = null;

                if (model[i].EstaSeleccionado && !(await gestionUsuarios.IsInRoleAsync(user, rol.Name)))
                {
                    result = await gestionUsuarios.AddToRoleAsync(user, rol.Name);
                }
                else if(!model[i].EstaSeleccionado && await gestionUsuarios.IsInRoleAsync(user, rol.Name))
                {
                    result = await gestionUsuarios.RemoveFromRoleAsync(user, rol.Name);
                } 
                else
                {
                    continue;
                }

                if (result.Succeeded)
                {
                    if (i < (model.Count -1))
                    {
                        continue;
                    }
                    else
                    {
                        return RedirectToAction("EditarRol", new { Id = rolId });
                    }
                }
            }

            return RedirectToAction("EditarRol", new { Id = rolId });
        }

        [HttpGet]
        [Route("Administracion/ListaUsuarios")]
        public IActionResult ListaUsuarios()
		{
            var usuarios = gestionUsuarios.Users;
            return View(usuarios);
		}

        [HttpGet]
        [Route("Administracion/EditarUsuario")]
        public async Task<IActionResult> EditarUsuario(string id)
		{
            var usuario = await gestionUsuarios.FindByIdAsync(id);

			if (usuario == null)
			{
                ViewBag.ErrorMessage = $"El usuario con Id = {id} no fue encontrado";
                return View("Error");
			}

            var usuarioClaims = await gestionUsuarios.GetClaimsAsync(usuario);

            var usuarioRoles = await gestionUsuarios.GetRolesAsync(usuario);

            var model = new EditarUsuarioModelo
			{
                Id = usuario.Id,
                Email = usuario.Email,
                NombreUsuario = usuario.UserName,
                ayudaPass = usuario.ayudaPass,
                Notificaciones = usuarioClaims.Select(c => c.Value).ToList(),
                Roles = usuarioRoles
			};

            return View(model);
		}

        [HttpPost]
        [Route("Administracion/EditarUsuario")]
		public async Task<IActionResult> EditarUsuario(EditarUsuarioModelo model)
		{
            var usuario = await gestionUsuarios.FindByIdAsync(model.Id);

			if (usuario == null)
			{
                ViewBag.ErrorMessage = $"El usuario con Id = {model.Id} no fue encontrado";
                return View("Error");
            }
			else
			{
                usuario.Email = model.Email;
                usuario.UserName = model.NombreUsuario;
                usuario.ayudaPass = model.ayudaPass;

                var resultado = await gestionUsuarios.UpdateAsync(usuario);

                if (resultado.Succeeded)
				{
                    return RedirectToAction("ListaUsuarios");
				}

                foreach (var error in resultado.Errors)
				{
                    ModelState.AddModelError("", error.Description);
				}

                return View(model);
			}
		}

        [HttpPost]
        [Route("Administracion/BorrarUsuario")]
        public async Task<IActionResult> BorrarUsuario(string id)
        {
            var user = await gestionUsuarios.FindByIdAsync(id);

            if (user == null)
            {
                ViewBag.ErrorMessage = $"El Usuario con el Id {id} no fue encontrado";
                return View("Error");
            }
            else
            {
                var resultado = await gestionUsuarios.DeleteAsync(user);

                if (resultado.Succeeded)
                {
                    return RedirectToAction("ListaUsuarios");
                }

                foreach (var error in resultado.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                return View("ListaUsuarios");
            }
        }

        [HttpPost]
        [Route("Administracion/BorrarRol")]
        [Authorize(Policy = "BorrarRolPolicy")]
        public async Task<IActionResult> BorrarRol(string id)
        {
            var rol = await gestionRoles.FindByIdAsync(id);

            if (rol == null)
            {
                ViewBag.ErrorMessage = $"El Rol con el Id {id} no fue encontrado";
                return View("Error");
            }
            else
            {
                try
                {
                    var resultado = await gestionRoles.DeleteAsync(rol);

                    if (resultado.Succeeded)
                    {
                        return RedirectToAction("ListaRoles");
                    }

                    foreach (var error in resultado.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }

                    return View("ListaRoles");
                }
                catch (DbUpdateException ex)
                {
                    log.LogError($"Se produjo un error al borrar el rol: {ex}");
                    ViewBag.ErrorTitle = $"El rol {rol.Name} está siendo utilizado";
                    ViewBag.ErrorMessage = $"El rol {rol.Name} no puede ser borrado porque contiene usuarios. Antes de borrar el rol quita los usuarios de dicho rol.";
                    return View("ErrorGenerico");
                }
            }
        }

        [HttpGet]
        [Route("Administracion/GestionarRolesUsuario")]
        [Authorize(Policy = "EditarRolPolicy")]
        public async Task<IActionResult> GestionarRolesUsuario(string IdUsuario)
        {
            ViewBag.IdUsuario = IdUsuario;

            var user = await gestionUsuarios.FindByIdAsync(IdUsuario);

            if (user == null)
            {
                ViewBag.ErrorMessage = $"El usuario con id = {IdUsuario} no se encontró";
                return View("Error");
            }

            var model = new List<RolUsuarioModelo>();

            foreach (var rol in gestionRoles.Roles)
            {
                var rolUsuarioModelo = new RolUsuarioModelo
                {
                    RolId = rol.Id,
                    RolNombre = rol.Name
                };

                if (await gestionUsuarios.IsInRoleAsync(user, rol.Name))
                {
                    rolUsuarioModelo.EstaSeleccionado = true;
                }
                else
                {
                    rolUsuarioModelo.EstaSeleccionado = false;
                }

                model.Add(rolUsuarioModelo);
            }

            return View(model);
        }

        [HttpPost]
        [Route("Administracion/GestionarRolesUsuario")]
        [Authorize(Policy = "EditarRolPolicy")]
        public async Task<IActionResult> GestionarRolesUsuario(List<RolUsuarioModelo> model, string IdUsuario)
        {
            var usuario = await gestionUsuarios.FindByIdAsync(IdUsuario);

            if (usuario == null)
            {
                ViewBag.ErrorMessage = $"El usuario con id = {IdUsuario} no se encontró";
                return View("Error");
            }

            var roles = await gestionUsuarios.GetRolesAsync(usuario);
            var result = await gestionUsuarios.RemoveFromRolesAsync(usuario, roles);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "No podemos borrar usuario con roles");
                return View(model);
            }

            result = await gestionUsuarios.AddToRolesAsync(usuario,
                model.Where(x => x.EstaSeleccionado).Select(y => y.RolNombre));

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "No podemos añadir los roles al usuario seleccionado");
                return View(model);
            }

            return RedirectToAction("EditarUsuario", new { Id = IdUsuario});
        }

        [HttpGet]
        [Route("Administracion/GestionarUsuarioClaims")]
        public async Task<IActionResult> GestionarUsuarioClaims(string IdUsuario)
        {
            var user = await gestionUsuarios.FindByIdAsync(IdUsuario);

            if (user == null)
            {
                ViewBag.ErrorMessage = $"El usuario con id = {IdUsuario} no se encontró";
                return View("Error");
            }

            var existingUserClaims = await gestionUsuarios.GetClaimsAsync(user);

            var modelo = new UsuarioClaimsViewModel
            {
                idUsuario = IdUsuario
            };

            foreach (Claim claim in AlmacenClaims.todosLosClaims)
            {
                UsuarioClaim usuarioClaim = new UsuarioClaim
                {
                    tipoClaim = claim.Type
                };              

                if (existingUserClaims.Any(c => c.Type == claim.Type && c.Value == "true"))
                {
                    usuarioClaim.estaSeleccionado = true;
                }

                modelo.Claims.Add(usuarioClaim);
            }

            return View(modelo);
        }

        [HttpPost]
        [Route("Administracion/GestionarUsuarioClaims")]
        public async Task<IActionResult> GestionarUsuarioClaims(UsuarioClaimsViewModel modelo)
        {
            var usuario = await gestionUsuarios.FindByIdAsync(modelo.idUsuario);

            if (usuario == null)
            {
                ViewBag.ErrorMessage = $"El usuario con id = {modelo.idUsuario} no se encontró";
                return View("Error");
            }

            var claims = await gestionUsuarios.GetClaimsAsync(usuario);
            var result = await gestionUsuarios.RemoveClaimsAsync(usuario, claims);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "No se pueden borrar las claims de este usuario");
                return View(modelo);
            }

            result = await gestionUsuarios.AddClaimsAsync(usuario,
                modelo.Claims.Select(x => new Claim(x.tipoClaim, x.estaSeleccionado ? "true":"false")));

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "No se pueden agregar claims a este usuario");
                return View(modelo);
            }

            return RedirectToAction("EditarUsuario", new { Id = modelo.idUsuario });
        }
    }   
}
