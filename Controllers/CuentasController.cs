﻿using Ejemplo1ASP.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Ejemplo1ASP.Controllers
{
    [Authorize]
    public class CuentasController : Controller
    {
        private readonly UserManager<UsuarioAplicacion> gestionUsuarios;
        private readonly SignInManager<UsuarioAplicacion> gestionLogin;
        private readonly ILogger<CuentasController> log;

        public CuentasController(UserManager<UsuarioAplicacion> gestionUsuarios, SignInManager<UsuarioAplicacion> gestionLogin,
            ILogger<CuentasController> log)
        {
            this.gestionUsuarios = gestionUsuarios;
            this.gestionLogin = gestionLogin;
            this.log = log;
        }

        [HttpGet]
        [Route("Cuentas/Registro")]
        [AllowAnonymous]
        public IActionResult Registro()
        {
            return View();
        }

        [HttpPost]
        [Route("Cuentas/Registro")]
        [AllowAnonymous]
        public async Task<IActionResult> Registro(RegistroModelo model)
        {
            if (ModelState.IsValid)
            {
                var usuario = new UsuarioAplicacion
                {
                    UserName = model.Email,
                    Email = model.Email,
                    ayudaPass = model.AyudaPass
                };

                var resultado = await gestionUsuarios.CreateAsync(usuario, model.Password);

                if (resultado.Succeeded)
                {
                    var token = await gestionUsuarios.GenerateEmailConfirmationTokenAsync(usuario);

                    var linkConfirmacion = "https://localhost:44390/Cuentas/ConfirmarEmail?usuarioId=" + usuario.Id + "&token=" + WebUtility.UrlEncode(token);

                    log.Log(LogLevel.Error, linkConfirmacion);

					if (gestionLogin.IsSignedIn(User) && User.IsInRole("Administrador"))
					{
                        return RedirectToAction("ListaUsuarios", "Administracion");
					}

                    ViewBag.ErrorTitle = "Registro correcto";
                    ViewBag.ErrorMessage = "Antes de iniciar sesión confirma el registro clicando en el link del email recibido";
                    return View("Error");
                }

                foreach (var error in resultado.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        [Route("Cuentas/ConfirmarEmail")]
        public async Task<IActionResult> ConfirmarEmail(string usuarioId, string token)
        {
            if (usuarioId == null || token == null)
            {
                return RedirectToAction("index", "home");
            }

            var usuario = await gestionUsuarios.FindByIdAsync(usuarioId);
            if (usuario == null)
            {
                ViewBag.ErrorMessage = $"El usuario con id {usuarioId} es invalido";
                return View("ErrorGenerico");
            }
            var result = await gestionUsuarios.ConfirmEmailAsync(usuario, token);
            if (result.Succeeded)
            {
                return View();
            }

            ViewBag.ErrorTitle = "El email no pudo ser confirmado";
            return View("ErrorGenerico");
        }

        [HttpPost]
        [Route("Cuentas/CerrarSesion")]
        public async Task<IActionResult> CerrarSesion()
        {
            await gestionLogin.SignOutAsync();
            return RedirectToAction("index", "home");
        }

        [HttpGet]
        [Route("Cuentas/Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string urlRetorno)
        {
            LoginViewModelo modelo = new LoginViewModelo
            {
                UrlRetorno = urlRetorno,
                LoginExternos = (await gestionLogin.GetExternalAuthenticationSchemesAsync()).ToList()
            };

            return View(modelo);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Cuentas/LoginExterno")]
        public IActionResult LoginExterno(string proveedor, string UrlRetorno)
        {
            var redirectUrl = Url.Action("ExternalLoginCallback", "Cuentas",
                new { ReturnUrl = UrlRetorno });
            var properties = gestionLogin.ConfigureExternalAuthenticationProperties(proveedor, redirectUrl);
            return new ChallengeResult(proveedor, properties);
        }

        [AllowAnonymous]
        [Route("Cuentas/LoginExterno")]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            LoginViewModelo loginViewModelo = new LoginViewModelo
            {
                UrlRetorno = returnUrl,
                LoginExternos = (await gestionLogin.GetExternalAuthenticationSchemesAsync()).ToList()
            };

            if (remoteError != null)
            {
                ModelState.AddModelError(string.Empty, $"Error en el proveedor externo: {remoteError}");
                return View("Login", loginViewModelo);
            }

            var info = await gestionLogin.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ModelState.AddModelError(string.Empty, "Error cargando la información");
                return View("Login", loginViewModelo);
            }

            var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            UsuarioAplicacion usuario = null;

            if (email != null)
            {
                usuario = await gestionUsuarios.FindByEmailAsync(email);

                if(usuario != null && !usuario.EmailConfirmed)
                {
                    ModelState.AddModelError(string.Empty, "Email sin confirmar");
                    return View("Login", loginViewModelo);
                }

            }

            var loginResultado = await gestionLogin.ExternalLoginSignInAsync(info.LoginProvider,
                info.ProviderKey, isPersistent: false, bypassTwoFactor: true);

            if (loginResultado.Succeeded)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                if (email != null)
                {
                    if (usuario == null)
                    {
                        usuario = new UsuarioAplicacion
                        {
                            UserName = info.Principal.FindFirstValue(ClaimTypes.Email),
                            Email = info.Principal.FindFirstValue(ClaimTypes.Email)
                        };

                        await gestionUsuarios.CreateAsync(usuario);

                        var token = await gestionUsuarios.GenerateEmailConfirmationTokenAsync(usuario);

                        var linkConfirmacion = "https://localhost:44390/Cuentas/ConfirmarEmail?usuarioId=" + usuario.Id + "&token=" + WebUtility.UrlEncode(token);

                        log.Log(LogLevel.Error, linkConfirmacion);

                        ViewBag.ErrorTitle = "Registro correcto";
                        ViewBag.ErrorMessage = "Antes de iniciar sesión confirma el registro clicando en el link del email recibido";
                        return View("Error");
                    }

                    await gestionUsuarios.AddLoginAsync(usuario, info);
                    await gestionLogin.SignInAsync(usuario, isPersistent: false);

                    return LocalRedirect(returnUrl);
                }

                ViewBag.ErrorTitle = $"Email claim no fue recibido de: {info.LoginProvider}";
                ViewBag.ErrorMessage = "Contacta con vidalcerda17@gmail.com";

                return View("Error");
            }
        }

        [HttpPost]
        [Route("Cuentas/Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModelo model, string returnUrl)
        {
            model.LoginExternos = (await gestionLogin.GetExternalAuthenticationSchemesAsync()).ToList();

            if (ModelState.IsValid)
            {
                var usuario = await gestionUsuarios.FindByEmailAsync(model.Email);

                if (usuario != null && !usuario.EmailConfirmed &&
                    (await gestionUsuarios.CheckPasswordAsync(usuario, model.Password)))
                {
                    ModelState.AddModelError(string.Empty, "Email todavia no confirmado");
                    return View(model);
                }

                var result = await gestionLogin.PasswordSignInAsync(
                    model.Email, model.Password, model.Recuerdame, false);

                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("index", "home");
                    }
                }

                ModelState.AddModelError(string.Empty, "Inicio de sesión no válido");
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Cuentas/OlvidoPassword")]
        public IActionResult OlvidoPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Cuentas/OlvidoPassword")]
        public async Task<IActionResult> OlvidoPassword(OlvidoPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var usuario = await gestionUsuarios.FindByEmailAsync(model.Email);

                if (usuario != null && await gestionUsuarios.IsEmailConfirmedAsync(usuario))
                {
                    var token = await gestionUsuarios.GeneratePasswordResetTokenAsync(usuario);

                    var linkReseteaPass = "https://localhost:44390/Cuentas/ReseteaPassword?Email=" + model.Email + "&token=" + WebUtility.UrlEncode(token);

                    log.Log(LogLevel.Warning, linkReseteaPass);

                    return View("OlvidoPasswordConfirmacion");
                }
                return View("OlvidoPasswordConfirmacion");
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Cuentas/ReseteaPassword")]
        public IActionResult ReseteaPassword(string token, string email)
        {
            if (token == null || email == null)
            {
                ModelState.AddModelError("", "Token para resetear password incorrecto");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Cuentas/ReseteaPassword")]
        public async Task<IActionResult> ReseteaPassword(ResetPassViewModel model)
        {
            if (ModelState.IsValid)
            {
                var usuario = await gestionUsuarios.FindByEmailAsync(model.Email);

                if (usuario != null)
                {
                    var resultado = await gestionUsuarios.ResetPasswordAsync(usuario, model.Token, model.Password);
                    if (resultado.Succeeded)
                    {
                        return View("ReseteaPasswordConfirmacion");
                    }

                    foreach (var error in resultado.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(model);
                }
                return View("ReseteaPasswordConfirmacion");
            }
            return View(model);
        }

        [AcceptVerbs("Get", "Post")]
        [AllowAnonymous]
        [Route("Cuentas/ComprobarEmail")]
        public async Task<IActionResult> ComprobarEmail(string email)
        {
            var user = await gestionUsuarios.FindByEmailAsync(email);

            if (user == null)
            {
                return Json(true);
            }
            else
            {
                return Json($"El email {email} no está disponible.");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Cuentas/AccesoDenegado")]
        public IActionResult AccesoDenegado()
		{
            return View();
		}

        [HttpGet]
        [Route("Cuentas/CambiarPassword")]
        public IActionResult CambiarPassword()
        {
            return View();
        }

        [HttpPost]
        [Route("Cuentas/CambiarPassword")]
        public async Task<IActionResult> CambiarPassword(CambioPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var usuario = await gestionUsuarios.GetUserAsync(User);
                if (usuario == null)
                {
                    return RedirectToAction("Login");
                }

                var result = await gestionUsuarios.ChangePasswordAsync(usuario,
                    model.PasswordActual, model.NuevaPassword);

                if (!result.Succeeded)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return View();
                }

                await gestionLogin.RefreshSignInAsync(usuario);
                return View("CambiarPasswordConfirmacion");
            }

            return View(model);
        }
    }
}
