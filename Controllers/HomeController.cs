﻿using Ejemplo1ASP.Models;
using Ejemplo1ASP.Seguridad;
using Ejemplo1ASP.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;

namespace Ejemplo1ASP.Controllers
{
	[Authorize]
	public class HomeController:Controller
	{
		private IAmigoAlmacen amigoAlmacen;
		private IWebHostEnvironment hosting;
		private readonly IDataProtector protector;
		public HomeController(IAmigoAlmacen AmigoAlmacen, IWebHostEnvironment hostingEnvironment,
			IDataProtectionProvider dataProtectionProvider,
			ProteccionStrings proteccionStrings)
		{
			amigoAlmacen = AmigoAlmacen;
			hosting = hostingEnvironment;
			protector = dataProtectionProvider.CreateProtector(proteccionStrings.AmigoIdRuta);
		}
		
		[Route("")]
		[Route("Home")]
		[Route("Home/Index")]
		[AllowAnonymous]
		public ViewResult Index()
		{
			var modelo = amigoAlmacen.DameTodosLosAmigos().Select(e =>
			{
				e.IdEncriptado = protector.Protect(e.Id.ToString());
				return e;
			});
			return View(modelo);
		}

		[Route("Home/Details/{id?}")]
		[AllowAnonymous]
		public ViewResult Details(string id)
		{
			string IdDesencriptadoCadena = protector.Unprotect(id);
			int IdDesencriptado = Convert.ToInt32(IdDesencriptadoCadena);

			DetallesView detalles = new DetallesView();
			detalles.amigo = amigoAlmacen.DameDatosAmigo(IdDesencriptado);
			detalles.Titulo = "LISTA AMIGOS VIEW MODELS";
			detalles.SubTitulo = "UNA COSITA BONITA";

			if (detalles.amigo == null)
			{
				Response.StatusCode = 404;
				return View("AmigoNoEncontrado", IdDesencriptado);
			}

			return View(detalles);
		}

		[HttpGet]
		[Route("Home/Create")]
		public ViewResult Create()
		{
			return View();
		}

		[HttpPost]
		[Route("Home/Create")]
		public IActionResult Create(CrearAmigoModelo a)
		{
			if (ModelState.IsValid)
			{
				string guidImagen = null;
				if (a.Foto != null)
				{
					string ficherosImagenes = Path.Combine(hosting.WebRootPath, "images");
					guidImagen = Guid.NewGuid().ToString() + a.Foto.FileName;
					string rutaDefinitiva = Path.Combine(ficherosImagenes, guidImagen);
					a.Foto.CopyTo(new FileStream(rutaDefinitiva, FileMode.Create));
				}

				Amigo nuevoAmigo = new Amigo();
				nuevoAmigo.Nombre = a.Nombre;
				nuevoAmigo.Email = a.Email;
				nuevoAmigo.Ciudad = a.Ciudad;
				nuevoAmigo.rutaFoto = guidImagen;

				amigoAlmacen.nuevo(nuevoAmigo);
				return RedirectToAction("details", new { id = nuevoAmigo.Id });
			}

			return View();
		}

		[HttpGet]
		public ViewResult Edit(int id)
		{
			Amigo amigo = amigoAlmacen.DameDatosAmigo(id);
			EditarAmigoModelo amigoEditar = new EditarAmigoModelo
			{
				Id = amigo.Id,
				Nombre = amigo.Nombre,
				Email = amigo.Email,
				Ciudad = amigo.Ciudad,
				rutaFotoExistente = amigo.rutaFoto
			};
			return View(amigoEditar);
		}

		[HttpPost]
		public IActionResult Edit(EditarAmigoModelo model)
		{
			if (ModelState.IsValid)
			{
				Amigo amigo = amigoAlmacen.DameDatosAmigo(model.Id);

				amigo.Nombre= model.Nombre;
				amigo.Email= model.Email;
				amigo.Ciudad= model.Ciudad;

				if (model.Foto != null)
				{
					if (model.rutaFotoExistente != null)
					{
						string ruta = Path.Combine(hosting.WebRootPath, "images", model.rutaFotoExistente);
						System.IO.File.Delete(ruta);
					}

					amigo.rutaFoto = SubirImagen(model);
				}

				Amigo amigoModificado = amigoAlmacen.modificar(amigo);

				return RedirectToAction("Index");
			}

			return View(model);
		}

		private string SubirImagen(EditarAmigoModelo model)
		{
			string nombreFichero = null;

			if (model.Foto != null)
			{
				string carpetaSubida = Path.Combine(hosting.WebRootPath, "images");
				nombreFichero = Guid.NewGuid().ToString() + "_" + model.Foto.FileName;
				string ruta = Path.Combine(carpetaSubida, nombreFichero);
				using (var fileStream = new FileStream(ruta, FileMode.Create))
				{
					model.Foto.CopyTo(fileStream);
				}
			}

			return nombreFichero;
		}
	}
}
