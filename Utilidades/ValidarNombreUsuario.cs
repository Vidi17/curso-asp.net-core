﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ejemplo1ASP.Utilidades
{
    public class ValidarNombreUsuario: ValidationAttribute
    {
        private readonly string usuario;
        public ValidarNombreUsuario(string usuario)
        {
            this.usuario = usuario;
        }

        public override bool IsValid(object value)
        {
            Boolean permitido = true;
            if (value.ToString().Contains("joder"))
            {
                permitido = false;
            }
            return permitido;
        }
    }
}
